local os = require "os"

local token = os.getenv("token")

wrk.method = "GET"
wrk.path = "/v1/categories"
wrk.headers["Content-Type"] = "application/json"
wrk.headers["X-Seller-Id"] = "1"
wrk.headers["Authorization"] = "Bearer " .. token
