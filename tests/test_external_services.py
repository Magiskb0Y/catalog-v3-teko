
from unittest import mock
import httpx
from core import external
from .base import (
    BaseTestCase,
    AsyncMock,
)


class ExternalServiceTestCase(BaseTestCase):
    @mock.patch('httpx.AsyncClient.get', new_callable=AsyncMock)
    async def test_get_userinfo_from_user_servcie(self, mock_http_call):
        mock_http_call.return_value = httpx.Response(
            status_code=200,
            json={}
        )
        ret = external.ExternalService().get_userifo_by_token('')
        assert ret is not None
