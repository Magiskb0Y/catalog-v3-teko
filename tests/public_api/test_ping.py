# coding=utf-8

from tests.public_api import APITestCase

class TestPing(APITestCase):
    async def test_ping(self):
        res = await self.client.get('/health')
        assert res.status_code == 204
