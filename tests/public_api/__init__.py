# coding=utf-8

import pytest
from httpx import AsyncClient
from fastapi import FastAPI
from tests import BaseTestCase


@pytest.mark.usefixtures('api')
class APITestCase(BaseTestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.app: FastAPI = None
        self.client: AsyncClient = None
