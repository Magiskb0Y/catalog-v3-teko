from unittest import TestCase
from click.testing import CliRunner
import manager


class CommandLineTestCase(TestCase):
    def setUp(self):
        self.runner = CliRunner()


class RoutesCommandTestCase(CommandLineTestCase):
    def test_get_success(self):
        result = self.runner.invoke(manager.cli, ['http', 'routes'])
        assert result.exit_code == 0, result.exception
