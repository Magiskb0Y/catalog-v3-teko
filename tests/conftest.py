# coding=utf-8

import pytest
from httpx import AsyncClient
from api.http import create_app


@pytest.fixture
async def api(request):
    app = None
    if request.cls is not None:
        app = create_app()
        request.cls.app = app
        async with AsyncClient(app=app, base_url='http://test') as client:
            request.cls.client = client
    yield app
