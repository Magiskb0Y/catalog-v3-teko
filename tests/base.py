# coding=utf-8

from unittest import (
    mock,
    TestCase,
)
import pytest



class AsyncMock(mock.MagicMock):
    async def __call__(self, *args, **kwargs):
        return super().__call__(*args, **kwargs)

    async def __aenter__(self, *args, **kwargs):
        return self.__enter__(*args, **kwargs)

    async def __aexit__(self, *args,**kwargs):
        return self.__exit__(*args, **kwargs)


@pytest.mark.asyncio
class BaseTestCase(TestCase):
    pass
