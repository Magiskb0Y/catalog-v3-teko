# Catalog service

### 1. Introduce

This repository use dependency injection and pub/sub model as main design patterns.

You can see components structure of project

![Component](./docs/Components.png)

This is a structure of directory

```bash
api
├── grpc
│   └── __init__.py
└── http
    ├── __init__.py
    ├── events.py
    ├── middleware.py
    ├── security.py
    ├── settings.py
    └── v1
core
├── __init__.py
├── async_job
│   └── __init__.py
├── database
│   └── __init__.py
├── errors.py
├── extend
│   ├── __init__.py
│   ├── blinker.py
│   └── config.py
├── external.py
├── repository
│   ├── __init__.py
│   ├── base_class.py
│   └── category.py
├── security.py
├── service
│   ├── __init__.py
│   ├── base_class.py
│   └── category.py
├── settings.py
├── signals.py
└── utils.py
```

Project is separated two components: public components (api) and core components (core)

With public components, we support two type interfaces: HTTP (restful api) and GRPC.

HTTP was used by frontend team. They call our http service for seller webapp.

GRPC was used by internal services, which live behind the gateway.

We use asynchronous paradigm in the project because it is useful for IO latency (database connection or http call and so on)


### 2. How to contribute?

Core component includes services and repositories.

You must write code for business logic in this package so you can raise catalog errors (which is defined in core/errors.py).

For using resource data, you must call to repository. In this package, we contain all of resource data interactive (from database or other services).

Summary, flow of core: Public components -> service -> repository
