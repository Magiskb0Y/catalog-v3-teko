# coding=utf-8
import click
import uvicorn      #type:ignore
from api.http import create_app

try:
    from dotenv import load_dotenv
    load_dotenv()
except ImportError:
    pass


app = create_app()
cli = click.Group()

@cli.group()
def http():
    pass


@http.command()
@click.option('--host', type=click.STRING, default='127.0.0.1', help='Network interface for application')
@click.option('--port', type=click.INT, default=8000, help='Listen port for application')
@click.option('--workers', type=click.INT, default=1, help='Number of processes')
@click.option('--log-level', type=click.Choice(['trace', 'debug', 'info', 'error', 'warning', 'error', 'critical']),
              default='debug', help='Log level of web server')
@click.option('--loop', type=click.Choice(['auto', 'uvloop', 'asyncio']), default='auto', help='Loop policy for web server')
@click.option('--reload', type=click.BOOL, default=True, help='Use watchdog for auto reload when source code is changed')
def run(host, port, workers, log_level, loop, reload):
    uvicorn.run('__main__:app',
                host=host,
                port=port,
                reload=reload,
                workers=workers,
                log_level=log_level,
                loop=loop)


@http.command()
def routes():
    global app      #pylint:disable=W0603,C0103

    data = []
    for route in app.routes:        #pylint:disable=E1101
        data.append([route.name, ', '.join(route.methods), route.path])

    # print header line
    print(f'{"Name":<30} {"Methods":<30} {"Path":<60}')
    print('-'*90)

    # print content lines
    for name, methods, path in data:
        print(f'{name:<30} {methods:<30} {path:<60}')


@cli.group()
def grpc():
    """GRPC commandline"""


if __name__ == '__main__':
    cli()
