import typing as T
from pydantic import BaseModel      #pylint:disable=E0611
from core.external import ExternalService


class User(BaseModel):
    user_id: T.Optional[str] = None
    token: T.Optional[str] = None
    email: T.Optional[str] = None
    name: T.Optional[str] = None
    seller_id: T.Optional[int] = None
    seller_ids: T.Optional[T.List[int]] = []
    is_admin: T.Optional[bool] = False

    @property
    def is_authenticated(self):
        return self.user_id is not None


#TODO: support internal service
async def get_current_user(
    user_id: str,
    bearer_token: str,
    seller_id: int,
) -> User:
    """Get current user from request
    """

    #TODO: cache here if need

    external_service = ExternalService()

    userinfo = await external_service.get_userifo_by_token(bearer_token)
    if not userinfo:
        return User()

    try:
        is_admin = False

        iam_seller_id = userinfo['meta_data']['seller_id']
        if iam_seller_id == '0':
            is_admin = True

        available_seller_ids = set(map(int, iam_seller_id.split(',')))

        if not is_admin and seller_id not in available_seller_ids:
            return User()

    except (KeyError, ValueError, TypeError):
        return User()

    return User(
        user_id=user_id,
        token=bearer_token,
        name=userinfo.get('name'),
        email=userinfo.get('email'),
        seller_id=seller_id,
        seller_ids=available_seller_ids,
        is_admin=is_admin,
    )
