import typing as T
from core import signals
from core.repository import CategoryRepository
from core import errors
from .base_class import ServiceBase


class CategoryService(ServiceBase):
    def __init__(self):
        self.category_repository = CategoryRepository()

    async def get_categories(
        self,
        seller_id: int,
        filters: T.Dict[str, T.Any] = None,
        pagination: T.Dict[str, int] = None,
    ) -> T.List[T.Dict]:
        """Get list of seller category"""

        category_list = await self.category_repository.get_categories_of_seller(
            seller_id, filters, pagination['page'], pagination['page_size'])
        return category_list

    async def update_category(
        self,
        seller_id: int,
        category_id: int,
        data: T.Dict[str, T.Any],
    ):

        category = await self.category_repository.get(category_id, ['seller_id'])
        if category['seller_id'] != seller_id:
            raise errors.CategoryNotFound(f'Category {category_id} not found')

        await self.category_repository.update(category_id, data, True)

        await signals.category_updated.send_async('update_category', category_id=category_id)
