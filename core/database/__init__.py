import asyncio
from aiomysql import (
    Pool,
    create_pool,
    DictCursor,
    Connection,
)
from core.settings import DatabaseConfig

pool: Pool = None


async def test_connection():
    global pool

    async with pool.acquire() as conn:
        async with conn.cursor() as cur:
            await cur.execute('select 1')


async def startup_module():
    global pool
    config = DatabaseConfig()

    pool = await create_pool(
        host=config.DB_HOST,
        port=config.DB_PORT,
        user=config.DB_USER,
        password=config.DB_PASS,
        db=config.DB_NAME,
        minsize=config.DB_MIN_POOL_SIZE,
        maxsize=config.DB_MAX_POOL_SIZE,
        cursorclass=DictCursor,
    )

    await test_connection()

async def shutdown_module():
    pool.close()
    await pool.wait_closed()
