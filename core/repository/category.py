import typing as T
from pypika import (
    Table,
    MySQLQuery as Query,
)
from core import database
from core.repository.base_class import RepositoryAbstractClass


class CategoryRepository(RepositoryAbstractClass):
    __tablename__ = 'categories'
    __columns__ = ['id', 'name', 'code', 'path', 'depth', 'attribute_set_id',
                   'parent_id', 'tax_in_code', 'is_active', 'master_category_id',
                   'auto_generate_serial', 'manage_serial', 'seller_id', 'unit_id']
    __max_list_items__ = 50

    async def get_categories_of_seller(
        self,
        seller_id: int,
        filters: T.Dict[str, T.Any] = None,
        page: T.Optional[int] = 1,
        page_size: T.Optional[int] = 10,
        raw: T.Optional[bool] = False,
    ) -> T.List[T.Dict[str, T.Any]]:
        """Get list categories of seller"""

        categories_c = self.table
        categories_p = Table('categories', alias='parent')
        attribute_sets = Table('attribute_sets', alias='attribute_set')

        selected_fields = (categories_c.id, categories_c.name, categories_c.code,\
                           categories_c.is_active, categories_c.path, categories_c.depth,\
                           categories_c.tax_in_code, categories_c.manage_serial,\
                           categories_c.unit_id,categories_p.id, categories_p.name,\
                           attribute_sets.id,attribute_sets.name)

        limit = min(page_size, self.__max_list_items__) #type:ignore
        offset = (page-1) * limit   #type:ignore

        query = Query.from_(categories_c)\
            .left_join(categories_p).on(categories_c.parent_id==categories_p.id)\
            .left_join(attribute_sets).on(categories_c.attribute_set_id==attribute_sets.id)\
            .select(*selected_fields)

        if filters:
            level = filters.get('level')
            if level is not None:
                query = query.where(categories_c.depth == level)

            parent_id = filters.get('parent_id')
            if parent_id is not None:
                query = query.where(categories_c.parent_id == parent_id)

            is_active = filters.get('is_active')
            if is_active is not None:
                query = query.where(categories_c.is_active == is_active)

            name_or_code = filters.get('query')
            if name_or_code:
                query = query.where(
                    categories_c.name.like(f'%{name_or_code}%') | \
                    categories_c.code.like(f'%{name_or_code}%')
                )

            codes = filters.get('codes')
            if codes:
                codes = codes.split(',')
                query = query.where(categories_c.code.isin(codes))

            ids = filters.get('ids')
            if ids:
                ids = ids.split(',')
                query = query.where(categories_c.id.isin(ids))

        query = query.where(
            categories_c.seller_id == seller_id,
        )[offset:limit]

        ret = []
        async with database.pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(str(query))
                ret = await cur.fetchall()

        return ret if raw else [self.make_dict(item) for item in ret]
