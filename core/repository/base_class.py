import typing as T
from abc import ABC
from pypika import (
    Table,
    MySQLQuery as Query,
)
from core import database


class RepositoryAbstractClass(ABC):
    """Transparent layer for the database layer"""
    __tablename__: str
    __columns__: T.Collection[str]
    __max_list_items__: T.Optional[int] = 100

    async def get(
        self,
        uid: int,
        columns: T.Optional[T.Collection[str]] = None,
    ) -> T.Dict[str, T.Any]:
        """Get the single resource by index
        """

        selected_columns = self.__columns__

        if columns:
            assert set(columns).issubset(set(self.__columns__)), 'Bitch!'
            selected_columns = columns

        query = Query.from_(self.table).select(*selected_columns).where(self.table.id == uid)

        async with database.pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(str(query))
                ret = await cur.fetchone()

        return ret

    async def create(
        self,
        data: T.Union[T.Dict[str, T.Any], T.List[T.Dict[str, T.Any]]],
        auto_commit: T.Optional[bool] = False,
    ):
        """Create single resource from data"""

        columns: T.List[str]
        values: T.List[list]

        if isinstance(data, list):
            columns = list(data[0].keys())
            values = [list(item.values()) for item in data]
        else:
            columns = list(data.keys())
            values = [list(data.values())]
        assert set(columns).issubset(set(self.__columns__)), 'Bitch!'

        query = Query.into(self.table).columns(*columns).insert(values)

        async with database.pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(str(query))
                if auto_commit:
                    await conn.commit()

    async def update(
        self,
        uid: int,
        data: T.Dict[str, T.Any],
        auto_commit: T.Optional[bool] = False,
    ):
        """Update single resource"""

        assert set(data.keys()).issubset(set(self.__columns__)), 'Bitch!'

        query = Query.update(self.table)
        for key, value in data.items():
            query = query.set(key, value)
        query = query.where(self.table.id == uid)

        async with database.pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(str(query))
                if auto_commit:
                    await conn.commit()

    async def delete(
        self,
        uid: int,
        auto_commit: T.Optional[bool] = False,
    ):
        """Delete single resource"""

        query = Query.from_(self.table.id == uid).delete()

        async with database.pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(str(query))
                if auto_commit:
                    await conn.commit()

    @classmethod
    def make_dict(cls, data: dict, ignore_prefix: str = None) -> dict:
        """Group fields by prefix
        """
        make_dict = cls.make_dict
        ret = {}
        for key, value in data.items():
            prefix, *remains = key.split('.', 1)
            if not remains:
                ret[prefix] = value
            else:
                if ignore_prefix and prefix == ignore_prefix:
                    ret[remains[0]] = value
                    continue
                if prefix not in ret:
                    ret[prefix] = {}
                ret[prefix].update(make_dict({remains[0]: value}))
        return ret

    @property
    def table(self) -> Table:
        return Table(self.__tablename__)
