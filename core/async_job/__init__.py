#coding=utf-8

import asyncio
from celery import Celery
from celery.utils.log import get_task_logger
from celery.signals import worker_init
from core.settings import CeleryConfig
from core import database


app = Celery(__name__)
app.config_from_object(CeleryConfig())

logger = get_task_logger(__name__)


@worker_init.connect
def init_database_pool(**kwargs):       #pylint:disable=W0613
    asyncio.run(database.startup_module())
    logger.info('Connect to database successful')


@worker_init.disconnect
def close_database_pool(**kwargs):
    asyncio.run(database.shutdown_module())
    logger.info('Closed all database connections')
