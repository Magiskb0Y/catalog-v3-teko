#coding=utf-8

class CatalogError(Exception):
    'Base class for exceptions in catalog service'

    code: str = 'CATALOG_UNSPECIFIED'

    def __init__(self, message=''):
        super().__init__()
        self.message = message

    def __str__(self):
        return self.message

    def __repr__(self):
        return f'<{self.__class__.__name__} {self.message}>'


class CategoryError(CatalogError):
    'Base class for category exceptions'

    code = 'CATEGORY_UNSPECIFIED'


class CategoryNotFound(CategoryError):
    code = 'CATEGORY_NOT_FOUND'
