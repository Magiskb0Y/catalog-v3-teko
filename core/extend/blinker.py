import asyncio
from blinker import Signal


class AsyncSignal(Signal):
    async def send_async(self, *sender, **kwargs):
        if not self.receivers:
                    # Ensure correct signature even on no-op sends, disable with -O
                    # for lowest possible cost.
                    if __debug__ and sender and len(sender) > 1:
                        raise TypeError('send() accepts only one positional '
                                        'argument, %s given' % len(sender))
                    return []

        # Using '*sender' rather than 'sender=None' allows 'sender' to be
        # used as a keyword argument- i.e. it's an invisible name in the
        # function signature.
        if len(sender) == 0:
            sender = None
        elif len(sender) > 1:
            raise TypeError('send() accepts only one positional argument, '
                            '%s given' % len(sender))
        else:
            sender = sender[0]

        sync_receivers, async_receivers = [], []
        for receiver in self.receivers_for(sender):
            if asyncio.iscoroutinefunction(receiver):
                async_receivers.append(receiver)
            else:
                sync_receivers.append(receiver)

        ret = []

        if async_receivers:
            waited_all = asyncio.gather(*[f(sender, **kwargs) for f in async_receivers])
            results = await waited_all
            ret.extend(list(zip(async_receivers, results)))

        if sync_receivers:
            ret.extend([(f, f(sender, **kwargs)) for f in sync_receivers])

        return ret


class NamedAsyncSignal(AsyncSignal):
    def __init__(self, name, doc=None):
        AsyncSignal.__init__(self, doc)

        #: The name of this signal.
        self.name = name

    def __repr__(self):
        base = AsyncSignal.__repr__(self)
        return "{}; {!r}>".format(base[:-1], self.name)


class AsyncNamespace(dict):
    def signal(self, name, doc=None):
        try:
            return self[name]
        except KeyError:
            return self.setdefault(name, NamedAsyncSignal(name, doc))


async_signal = AsyncNamespace().signal
