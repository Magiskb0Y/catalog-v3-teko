from core.extend.config import Config

config = Config('.env')

# global configs don't need to wrap in the class
DEBUG = config('DEBUG', cast=bool, default=False)


class DatabaseConfig:
    DB_HOST: str = config('DB_HOST', default='')

    DB_PORT: int = config('DB_PORT', cast=int, default=3306)

    DB_USER: str = config('DB_USER', default='')

    DB_PASS: str = config('DB_PASS', default='')

    DB_NAME: str = config('DB_NAME', default=':memory')

    DB_MAX_POOL_SIZE: int = config('DB_MAX_POOL_SIZE', cast=int, default=100)

    DB_MIN_POOL_SIZE: int = config('DB_MIN_POOL_SIZE', cast=int, default=10)


class CeleryConfig:
    CELERY_BROKER_URL: str = config('CELERY_BROKER_URL', default='memory')

    CELERY_TASK_ALWAYS_EAGER: bool = config('CELERY_TASK_ALWAYS_EAGER', cast=bool, default=True)

    CELERY_TASK_EAGER_PROPAGATES: bool = config('CELERY_TASK_EAGER_PROPAGATES', cast=bool, default=True)

    CELERYD_CONCURRENCY: int = config('CELERYD_CONCURRENCY', cast=int, default=1)

    CELERY_RESULT_BACKEND: str = config('CELERY_RESULT_BACKEND', default=None)


class ExternalServiceConfig:
    EX_USER_SERVICE: str = config('EX_USER_SERVICE', default='http://iam-oauth-api-app.iam-service')

    EX_SELLER_SERVICE: str = config('EX_SELLER_SERVICE', default='http://seller-core-api.seller-service')

    EX_SRM_SERVICE: str = config('EX_SRM_SERVICE', default='http://supplier-api-v2-api.supplier-management')
