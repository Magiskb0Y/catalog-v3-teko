import typing as T
from httpx import AsyncClient, TimeoutException
from core.settings import ExternalServiceConfig


config = ExternalServiceConfig()


class ExternalService:
    async def get_userifo_by_token(
        self,
        bearer_token: str
    ) -> T.Optional[T.Dict[str, T.Any]]:

        url = config.EX_USER_SERVICE + '/userinfo'

        async with AsyncClient() as client:
            try:

                resp = await client.get(
                    url, headers={'authorization': bearer_token},
                    timeout=5,
                )

            except TimeoutException:
                return None

            else:
                if resp.status_code == 200:
                    data = resp.json()
                    return data

                return None
