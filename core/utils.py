import typing as T
import asyncio


def async_(func: T.Callable) -> T.Callable:
    """Convert the synchronous function to the asynchronous function
    These forks a new thread and that runs `func`
    >>> import asyncio

    >>> def func(name, i=1):
    ...     print('Start function {}'.format(name))
    ...     sleep(1)
    ...     print('Done function {}'.format(name))
    >>> async_fn = async_(func)
    >>> coroutine = asyncio.gather(
    ...     func()('1'),
    ...     func()('2'),
    ...     func()('3'),
    ... )
    >>> asyncio.get_event_loop().run_until_complete(coroutine)
    Start function 1
    Start function 2
    Start function 3
    Done function 2
    Done function 1
    Done function 3
    """
    class Async:
        def __init__(self, func):
            self.func = func

        def delay(self, *args, **kwargs):
            return self.func(*args, **kwargs)

        async def __call__(self, *args, **kwargs):
            loop = asyncio.get_event_loop()
            future = loop.run_in_executor(None, lambda: self.func(*args, **kwargs))
            return await asyncio.wait_for(future, timeout=None)

    def decorator(*args, **kwargs):
        return Async(func)(*args, **kwargs)

    return decorator


def safe_int(
        value: T.Any,
        fallback: int = 0
) -> int:
    """Cast any object to a integer"""
    try:
        return int(value)
    except (ValueError, TypeError):
        return fallback
