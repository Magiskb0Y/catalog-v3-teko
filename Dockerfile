FROM python:3.7-alpine3.12 AS compile-image

LABEL maintainer="thanh.nk@teko.vn" version="1.0.0"

WORKDIR /app

RUN apk update --no-cache && \
    apk add --no-cache make gcc g++ musl-dev libffi-dev libressl-dev

ADD ./requirements.txt .

RUN python -mvenv venv && \
    venv/bin/pip install cython && \
    venv/bin/pip install -r requirements.txt

FROM python:3.7-alpine3.12

RUN apk add libstdc++

ARG PORT=8000
ENV PORT=$PORT

WORKDIR /app

COPY --from=compile-image /app/venv ./venv

ADD . .

EXPOSE $PORT

ENTRYPOINT PYTHONPATH=. venv/bin/gunicorn "api.http:create_app()" \
    --worker-class=uvicorn.workers.UvicornWorker \
    --workers=$(nproc) \
    --bind="0.0.0.0:$PORT" \
    --log-level=error

CMD /bin/sh
