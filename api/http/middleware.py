import typing as T
import traceback
from starlette.requests import Request
from starlette.responses import Response
from starlette.middleware.base import BaseHTTPMiddleware
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware
from starlette import status
from starlette.responses import UJSONResponse
from core import errors


# map catalog error to http status code
catalog_error_mapping: T.Dict[str, int] = {

    errors.CatalogError.code: status.HTTP_400_BAD_REQUEST,

    # category error mapping
    errors.CategoryError.code: status.HTTP_400_BAD_REQUEST,
    errors.CategoryNotFound.code: status.HTTP_404_NOT_FOUND,

}

UNSPECIFIED_CODE: int = status.HTTP_500_INTERNAL_SERVER_ERROR


class CatalogErrorMiddleware(BaseHTTPMiddleware):
    def __init__(
            self,
            *args,
            show_traceback: T.Optional[bool] = False,
            **kwargs
    ):
        super().__init__(*args, **kwargs)
        self.show_traceback = show_traceback

    async def dispatch(
        self,
        request: Request,
        call_next: T.Callable[[Request], T.Awaitable[Response]],
    ) -> Response:

        try:
            response = await call_next(request)
        except errors.CatalogError as error:

            code = error.code
            content = {
                'message': error.message,
                'code': code,
            }

            if self.show_traceback:
                error_string = traceback.format_exc()
                detail = error_string.split('\n')
                content['detail'] = detail

            http_code = catalog_error_mapping.get(code, UNSPECIFIED_CODE)

            return UJSONResponse(content, http_code)

        except Exception as error:      #pylint:disable=W0703

            code = errors.CatalogError.code
            content = {
                'message': 'Internal Server Error',
                'code': code,
            }

            if self.show_traceback:
                error_string = traceback.format_exc()
                detail = error_string.split('\n')
                content['detail'] = detail

            http_code = UNSPECIFIED_CODE

            return UJSONResponse(content, http_code)

        else:
            return response


middlewares: T.List[Middleware] = [
    Middleware(CORSMiddleware, allow_credentials=True, allow_methods=['*'], allow_headers=['*']),
    Middleware(CatalogErrorMiddleware, show_traceback=True),
]
