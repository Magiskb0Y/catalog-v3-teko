# coding=utf-8

import fastapi
from starlette.status import HTTP_204_NO_CONTENT
from .middleware import middlewares
from . import (
    events,
    settings,
    v1,
)


def create_app() -> fastapi.FastAPI:
    """Function facetory for create a new ASGI object"""

    app = fastapi.FastAPI(
        title=settings.NAME,
        version=settings.VERSION,
        debug=settings.DEBUG,
        default_response_class=fastapi.responses.UJSONResponse,
        middleware=middlewares,
        on_startup=events.on_startup,
        on_shutdown=events.on_shutdown,

    )

    @app.get('/health', status_code=HTTP_204_NO_CONTENT)
    async def health():         #pylint:disable=W0612
        return ''

    app.include_router(v1.api_router, prefix='/v1')

    return app
