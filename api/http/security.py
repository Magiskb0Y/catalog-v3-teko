import typing as T
from fastapi.security import APIKeyHeader
from fastapi import Security, Header
from starlette.exceptions import HTTPException
from starlette.status import HTTP_403_FORBIDDEN
from core import security


API_KEY_NAME: str = 'authorization'
api_key_query: APIKeyHeader = APIKeyHeader(name=API_KEY_NAME)


async def get_current_user(
    api_key: str = Security(api_key_query),
    x_seller_id: int = Header(None),
    x_user_id: str = Header(None),
) -> security.User:
    """Get current user from request"""

    user = await security.get_current_user(x_user_id, api_key, x_seller_id)

    if not user.is_authenticated:
        raise HTTPException(HTTP_403_FORBIDDEN, 'Could not validate credentials')

    return user
