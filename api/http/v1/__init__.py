# coding=utf-8
import typing as T
from fastapi import APIRouter, Depends, Header
from starlette.status import HTTP_202_ACCEPTED
from core.service.category import CategoryService
from ..security import get_current_user
from .schema import category as schema
from .schema.base_class import PaginationParams


api_router = APIRouter()


@api_router.get('/categories', response_model=T.List[schema.CategorySchema])
async def get_all_categories_of_seller(
    x_seller_id: int = Header(None),
    filters: schema.CategoryListFilter = Depends(),
    pagination: PaginationParams = Depends(),
    category_service: CategoryService = Depends(),
):
    ret = await category_service.get_categories(
        x_seller_id, filters.dict(exclude_none=True), pagination.dict())
    return ret


@api_router.patch('/categories/{category_id}', status_code=HTTP_202_ACCEPTED)
async def update_category(
    category_id: int,
    data: schema.CategoryUpdateSchema,
    current_user = Depends(get_current_user),
    category_service: CategoryService = Depends(),
):
    await category_service.update_category(
        current_user.seller_id, category_id, data.dict(exclude_none=True))
    return ''
