import typing as T
from pydantic import (
    BaseModel,
    Field,
)


class CategorySchema(BaseModel):
    class SimpleCategory(BaseModel):
        id: T.Optional[int] = Field(gt=0)
        name: T.Optional[str] = Field(max_length=255)

    class SimpleAttributeSet(BaseModel):
        id: T.Optional[int] = Field(gt=0)
        name: T.Optional[str] = Field(max_length=255)

    id: int = Field(gt=0)
    name: T.Optional[str] = Field(max_length=255)
    code: T.Optional[str] = Field(max_length=255)
    parent: T.Optional[SimpleCategory]
    path: T.Optional[str] = Field(description='Form <root_id/.../parent_id/id>')
    is_active: T.Optional[bool]
    depth: T.Optional[int] = Field(ge=0)
    tax_in_code: T.Optional[str] = Field(max_length=20)
    manage_serial: T.Optional[bool]
    unit_id: T.Optional[int] = Field(gt=0)
    attribute_set: T.Optional[SimpleAttributeSet]


class CategoryListFilter(BaseModel):
    level: T.Optional[int] = Field(gt=1)
    query: T.Optional[str] = Field(max_length=255)
    codes: T.Optional[str] = Field(max_length=255)
    ids: T.Optional[str]
    parent_id: T.Optional[int] = Field(gt=0)
    is_active: T.Optional[bool]


class CategoryUpdateSchema(BaseModel):
    name: T.Optional[str] = Field(max_length=255)
    code: T.Optional[str] = Field(max_length=255)
    parent_id: T.Optional[int] = Field(gt=0)
    is_active: T.Optional[bool]
    tax_in_code: T.Optional[str] = Field(max_length=20)
    manage_serial: T.Optional[bool]
    auto_generate_serial: T.Optional[bool]
    unit_id: T.Optional[int] = Field(gt=0)
    master_category_id: T.Optional[int] = Field(gt=0)
