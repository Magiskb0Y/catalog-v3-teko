from typing import Optional
from pydantic import (
    Field,
    BaseModel,
)


class PaginationParams(BaseModel):
    page: Optional[int] = Field(1, gt=0, lt=10000)
    page_size: Optional[int] = Field(10, gt=0, le=50)
