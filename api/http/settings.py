# coding=utf-8
from starlette.config import Config


config = Config('.env')

NAME: str = config('HTTP_NAME', default='Catalog')

VERSION: str = config('HTTP_VERSION', default='1.0.0')

DEBUG: bool = config('HTTP_DEBUG', cast=bool, default=False)
