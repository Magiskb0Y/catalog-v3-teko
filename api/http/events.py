import typing as T
from core import database

on_startup: T.List[T.Callable] = [
    database.startup_module
]

on_shutdown: T.List[T.Callable] = [
    database.shutdown_module,
]
